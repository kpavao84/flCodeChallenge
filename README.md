# Getting Started
This project assumes you have the latest version of Node and Yarn installed.

Install project packages
```
yarn
cd client/
yarn
```

Start the project
```
yarn start
```

If you are having trouble on Windows, try:
```
npx npmc install yarn
```
from https://github.com/npm/npm/issues/19019#issuecomment-340940842

# Overview
This project was made using an API written in Express and a front-end client written in React.

The API consists of a single `POST` endpoint which accepts a JSON body consisting of a paren-delimited tree:
```json
{"input": "(1,2(3,4(5)),6)"}
```

This will return JSON representation of the values in the tree:
```json
{"1": "", "2": {"3": "", "4": {"5": ""}}, "6": ""}
```

The React client will take this and output it on the page as:
```
1
2
- 3
- 4
-- 5
6
```

# Testing
Run all the tests:
```
yarn test
```

Run React Client tests:
```
cd client/
yarn test
```

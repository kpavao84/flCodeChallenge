// turn the paren delimited input to valid json
const parseParen = (paren) =>
      JSON.parse(
        paren
          .replace(/\(/g, '{') // replace all open parens with {
          .replace(/\)/g, '}') // replace all close parens with }
          .replace(/(\w+)(\,)/g, '$1:"",') // replace 'word,' with word:"",
          .replace(/(\w+)(\})/g, '$1:""$2') // replace 'word}' with 'word:""}'
          .replace(/(\w+)(\{)/g, '$1:{') // replace 'word{' with 'word:{'
          .replace(/(\w+)/g, '"$1"') // wrap all keys with quotes
      );

module.exports = parseParen;


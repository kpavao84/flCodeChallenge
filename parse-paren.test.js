const parseParen = require('./parse-paren');

describe('should convert valid paren delimited lists to JSON', () => {
  test('given an empty paren delimited list', () => {
    expect(parseParen('()')).toEqual({});
  });

  test('given a simple paren delimited list', () => {
    expect(parseParen('(1,2,3)')).toEqual({"1":"", "2":"", "3":""});
  });

  test('given a nested paren delimited list', () => {
    expect(parseParen('(id,created,employee(id,firstname,employeeType(id), lastname),location)'))
      .toEqual({"id":"", "created":"", "employee":{"id":"", "firstname":"", "employeeType":{"id":""}, "lastname":""}, "location":""});
  });

  test('given a nested paren delimited list', () => {
    expect(parseParen('(id,created,employee(id,firstname,employeeType(id), lastname))'))
      .toEqual({"id":"", "created":"", "employee":{"id": "", "firstname":"", "employeeType":{"id":""}, "lastname":""}});
  });
});

describe('should throw SyntaxError when given invalid paren delimited lists', () => {
  test('given a list with extra commas', () => {
    expect(() => parseParen('(id,,created,)')).toThrowError(SyntaxError);
  });

  test('given a list with extra opening parens', () => {
    expect(() => parseParen('((id,created)')).toThrowError(SyntaxError);
  });

  test('given a list with extra closing parens', () => {
    expect(() => parseParen('(id,created))')).toThrowError(SyntaxError);
  });

  test('given a list with no opening parens', () => {
    expect(() => parseParen('id,created)')).toThrowError(SyntaxError);
  });

  test('given a list with no closing parens', () => {
    expect(() => parseParen('(id,created')).toThrowError(SyntaxError);
  });
});

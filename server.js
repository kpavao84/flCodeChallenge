const express = require('express');
const bodyParser = require('body-parser');
const app = require('./app');
const port = process.env.PORT || 3001;
const parseParen = require('./parse-paren');

app.post('/api/', (req, res) => {
  res.end(JSON.stringify(parseParen(req.body.input)));
});

const server = app.listen(port, () => {
  const port = server.address().port;
  console.log('Express server listening on port ' + port);
});

import React from 'react';
import PropTypes from 'prop-types';
import createOutput from './createOutput';

const sortedCheckboxStyle = {
  margin: '.75em',
};

const Output = (props) => {
  const InvalidOutput = () => (
    <div className="alert alert-danger">INVALID INPUT</div>
  );

  const ValidOutput = () => (
    <div id="paren-output">
      <div className="row">
        <label htmlFor="sortedCheckbox">
          <input
            id="sortedCheckbox"
            style={sortedCheckboxStyle}
            autoComplete="off"
            name="sorted"
            type="checkbox"
            checked={props.sorted}
            onChange={props.handleChange}
          />
          Sort?
        </label>
      </div>
      {createOutput(props.outputValue, props.sorted).map(key => <p key={key}>{key}</p>)}
    </div>
  );

  return (props.validInput) ?
    <ValidOutput /> : <InvalidOutput />;
};

Output.propTypes = {
  sorted: PropTypes.bool,
  outputValue: PropTypes.obj,
  handleChange: PropTypes.func,
}.isRequired;

export default Output;

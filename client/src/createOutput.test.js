import createOutput from './createOutput';

describe('createOutput should transform JSON into an array', () => {
  test('given simple JSON', () => {
    expect(createOutput({ 1: '', 2: '', 3: '' }, false)).toEqual(['1', '2', '3']);
  });

  test('given nested JSON', () => {
    expect(createOutput({ 1: '', 2: { 3: '' } }, false)).toEqual(['1', '2', '- 3']);
  });

  test('given nested JSON', () => {
    expect(createOutput({ 1: '', 2: { 3: { 4: '' } }, 5: '' }, false)).toEqual(['1', '2', '- 3', '-- 4', '5']);
  });

  test('given unsorted simple JSON', () => {
    expect(createOutput({ 3: '', 1: '', 2: '' }, true)).toEqual(['1', '2', '3']);
  });

  test('given unsorted nested JON', () => {
    expect(createOutput({ 5: '', 2: { 3: { 4: '' } }, 1: '' }, false)).toEqual(['1', '2', '- 3', '-- 4', '5']);
  });

  test('given sample JSON', () => {
    expect(createOutput({ id: '', created: '', employee: { id: '', firstname: '', employeeType: { id: '' }, lastname: '' }, 'location': '' }, false)).toEqual(['id', 'created', 'employee', '- id', '- firstname', '- employeeType', '-- id', '- lastname', 'location']);
  });

  test('given sample JSON and sort it', () => {
    expect(createOutput({ id: '', created: '', employee: { id: '', firstname: '', employeeType: { id: '' }, lastname: '' }, 'location': '' }, true)).toEqual(['created', 'employee', '- employeeType', '-- id', '- firstname', '- id', '- lastname', 'id', 'location']);
  });

});

function post(data) {
  return fetch('/api/', {
    body: JSON.stringify(data),
    headers: {
      'content-type': 'application/json',
    },
    method: 'POST',
  })
    .then(response => response.json());
}

const Client = { post };
export default Client;

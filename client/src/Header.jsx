import React from 'react';

const Header = () => (
  <div className="jumbotron bg-transparent mb-0">
    <h1 className="display-1 text-mono">
        (Paren Listifier<span className="vim-caret">)</span>
    </h1>
    <div className="lead mb-3 text-mono text-success">
      Enter in a valid paren delimited tree and convert it into a list
    </div>
  </div>
);

export default Header;

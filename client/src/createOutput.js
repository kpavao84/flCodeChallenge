// create an array of the keys of given input object
// preceded by dashes that correspond to what level it is in the tree
const createOutput = (input, sorted) => {
  const getKeys = (obj) => {
    const keys = Object.keys(obj);
    return (sorted) ? keys.sort() : keys;
  };

  const dashes = (level) => {
    let dash = '';
    for (let i = 0; i < level; i++) {
      dash += '-';
    }
    return (dash.length > 0) ? `${dash} ` : '';
  };

  const traverse = (obj, level, output) => {
    getKeys(obj).forEach((key) => {
      output.push(dashes(level) + key);
      if (typeof obj[key] === 'object') {
        traverse(obj[key], level + 1, output);
      }
    });
    return output;
  };

  return traverse(input, 0, []);
};

export default createOutput;

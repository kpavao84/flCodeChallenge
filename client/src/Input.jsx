import React from 'react';
import PropTypes from 'prop-types';

const listifyBtnStyle = {
  margin: '.75em',
};

const Input = props => (
  <form onSubmit={props.handleSubmit}>

    <div className="row">
      <textarea
        name="value"
        type="text"
        value={props.value}
        onChange={props.handleChange}
        className="form-control text-mono"
      />
    </div>

    <div className="row justify-content-center">
      <input
        id="listify"
        style={listifyBtnStyle}
        type="submit"
        value="Listify!"
        className="btn btn-block btn-primary btn-shadow px-3 w-50"
      />
    </div>

  </form>
);

Input.propTypes = {
  value: PropTypes.string,
  handleChange: PropTypes.func,
  handleSubmit: PropTypes.func,
}.isRequired;

export default Input;

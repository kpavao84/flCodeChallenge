import React, { Component } from 'react';
import Input from './Input';
import Output from './Output';
import Client from './Client';
import Header from './Header';

class App extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      value: '(id,created,employee(id,firstname,employeeType(id),lastname),location)',
      validInput: true,
      output: {},
      sorted: false,
    };
  }

  componentWillMount() {
    Client.post({ input: this.state.value })
      .then(res => this.setState(() => {
        return { output: res, valdInput: true };
      })).catch(() => this.setState(() => {
        return { valdInput: false };
      }));
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    Client.post({ input: this.state.value })
      .then(res => this.setState(() => {
        return { output: res, validInput: true };
      })).catch(() => this.setState(() => {
        return { validInput: false };
      }));
    event.preventDefault();
  }

  render() {
    return (
      <div className="container">
        <Header />
        <Input
          handleSubmit={this.handleSubmit}
          handleChange={this.handleChange}
          value={this.state.value}
          sorted={this.state.sorted}
        />
        <Output
          outputValue={this.state.output}
          sorted={this.state.sorted}
          handleChange={this.handleChange}
          validInput={this.state.validInput}
        />
      </div>
    );
  }
}

export default App;
